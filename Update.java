import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;

/**
 * 
 * Folder: 1OBeny_o1b6NUW8ATfoOo5v02-Xszc4U9
 *
 */
public class DriveDemo {
	private static final String APPLICATION_NAME = "Google Drive Integration";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String[] partentFolders = new String[] { "1iD55P1WGCMLwSYe5lGL8P2YrBjpTrGpj" };
	
	private NetHttpTransport httpTransport;
	private Drive drive;
	
	public DriveDemo() {
		try {
			GoogleCredential credential = GoogleCredential
					.fromStream(DriveDemo.class.getResourceAsStream("/driveintegration.json"))
					.createScoped(Collections.singleton(DriveScopes.DRIVE));
		
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			drive = new Drive.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String... args) throws IOException, GeneralSecurityException {
		DriveDemo driveDemo = new DriveDemo();
		File uploadedFile = driveDemo.uploadBase64File("test.pdf", getBase64Data());
		// driveDemo.downloadFile(uploadedFile);
		driveDemo.listFiles();
		
		//driveDemo.createFolder();
		// driveDemo.resetPermissions();
	}

	/*
	private File uploadFile() throws IOException {
		java.io.File uploadFile = new java.io.File("test.pdf");
		File fileMetadata = new File();
		fileMetadata.setName(uploadFile.getName());
		FileContent mediaContent = new FileContent("application/pdf", uploadFile);
		File uploadedFile = drive.files().create(fileMetadata, mediaContent).execute();
		System.out.println("Uploaded: " + uploadedFile.getId() + ", " + uploadedFile.getName());
		return uploadedFile;
	}
	*/
	
	private File uploadBase64File(String fileName, String base64String) throws IOException {
		ByteArrayContent byteArrayContent = new ByteArrayContent("application/pdf", decodeBase64Data(base64String));
		File fileMetadata = new File();
		fileMetadata.setName(fileName);
		fileMetadata.setParents(Arrays.asList(partentFolders));
		File uploadedFile = drive.files().create(fileMetadata, byteArrayContent).execute();
		System.out.println("Uploaded: " + uploadedFile.getId() + ", " + uploadedFile.getName());
		return uploadedFile;
	}

	private void downloadFile(File uploadedFile) throws IOException {
		OutputStream out = new FileOutputStream(new java.io.File(uploadedFile.getId() + "_" + uploadedFile.getName()));
		drive.files().get(uploadedFile.getId()).executeMediaAndDownloadTo(out);
	}
	
	private static String getBase64Data() throws IOException {
		BufferedInputStream in = new BufferedInputStream(new FileInputStream("test.pdf"));
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buf = new byte[2048];
		int nbytes;
		while ((nbytes = in.read(buf)) != -1) {
			out.write(buf, 0, nbytes);
		}
		out.flush();
		in.close();
		
		return Base64.getEncoder().encodeToString(out.toByteArray());
	}
	
	private byte[] decodeBase64Data(String base64String) {
		return Base64.getDecoder().decode(base64String);
	}

	private void listFiles() throws IOException {
		// Print the names and IDs for up to 10 files.
		FileList result = drive.files().list().setPageSize(100).setFields("nextPageToken, files(id, name)").execute();
		List<File> files = result.getFiles();
		if (files == null || files.isEmpty()) {
			System.out.println("No files found.");
		} else {
			System.out.println("Files:");
			for (File file : files) {
				System.out.printf("%s (%s)\n", file.getName() + ", " + file.getMimeType() + ", " + file.getSize(), file.getId());
			}
		}
	}
	
	private void createFolder() throws IOException {
		File fileMetadata = new File();
		fileMetadata.setName("Drive Integration Folder");
		fileMetadata.setMimeType("application/vnd.google-apps.folder");
		File folder = drive.files().create(fileMetadata).execute();
		System.out.println("Folder: " + folder.getId() + ", " + folder.getName());
		
		// set up permissions
		String[] emailIds = new String[] { "nosaku@gmail.com", "integrationdrive929@gmail.com" };
		for (String emailId : emailIds) {
			Permission permission = new Permission();
			permission.setEmailAddress(emailId);
			permission.setType("user");
			permission.setRole("reader");
			drive.permissions().create(folder.getId(), permission).execute();
		}
	}

	/*
	 * private void resetPermissions() throws IOException { // set up permissions
	 * String[] emailIds = new String[] { "nosaku@gmail.com",
	 * "integrationdrive929@gmail.com" }; for (String emailId : emailIds) {
	 * Permission permission = new Permission();
	 * permission.setEmailAddress(emailId); permission.setType("user");
	 * permission.setRole("reader");
	 * drive.permissions().create("1OBeny_o1b6NUW8ATfoOo5v02-Xszc4U9",
	 * permission).execute(); } }
	 */
}
